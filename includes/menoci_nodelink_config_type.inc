<?php

function menoci_nodelink_config_type_edit($form, &$form_state, $type) {

  $form_state['node_link_type'] = $type;
  $nodelinktype = NodeLinkTypeRepository::findById($type);

  $renderer = new NodeLinkTypeForm($nodelinktype);

  return $renderer->getEditForm();
}

function menoci_nodelink_config_type_edit_submit($form, &$form_state) {
  $type = $form_state['node_link_type'];
  $nodelinktype = NodeLinkTypeRepository::findById($type);

  $form_handler = new NodeLinkTypeForm($nodelinktype);
  $form_handler->handleEditFormSubmit($form_state['values']);

  $form_state['redirect'] = MENOCI_NODELINK_URL_CONFIG;
}