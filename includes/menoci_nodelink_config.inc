<?php

function menoci_nodelink_config($form, &$form_state) {

  drupal_set_title(MENOCI_NODELINK_VAR_MODULE_TITLE . " Configuration");
  $form = NodeLinkConfigForm::getForm();

  return $form;
}

function menoci_nodelink_config_submit($form, &$form_state) {
  $inserted_type = NodeLinkConfigForm::handleSubmit($form_state['values']);
  if ($inserted_type) {
    drupal_set_message($inserted_type . " added successfully.");
    $form_state['redirect'] = NodeLinkType::url_by_type($inserted_type);
  }
  // by default: just return to the config form
}
