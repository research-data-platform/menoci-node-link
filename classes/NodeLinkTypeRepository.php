<?php


class NodeLinkTypeRepository {

  /**
   * @param string $type
   *
   * @return \NodeLinkType
   */
  public static function findById($type) {

    $nodelinktype = new NodeLinkType($type);
    $storage = variable_get(MENOCI_NODELINK_VAR_TYPE_STORAGE, []);

    if (array_key_exists($type, $storage)) {
      $config = $storage[$type][MENOCI_NODELINK_VAR_STORAGE_KEY_CONFIG];
      $nodelinktype->setListTitle($config[NodeLinkType::PARAM_LIST_TITLE]);
      $nodelinktype->setListTitleIconPath($config[NodeLinkType::PARAM_LIST_TITLE_ICON_PATH]);
      $nodelinktype->setListTableFields($config[NodeLinkType::PARAM_LIST_TABLE_FIELDS]);
    }
    return $nodelinktype;
  }

  public static function save(NodeLinkType $nodeLinkType) {
    $storage = variable_get(MENOCI_NODELINK_VAR_TYPE_STORAGE, []);

    $data = [
      NodeLinkType::PARAM_LIST_TITLE => $nodeLinkType->getListTitle(),
      NodeLinkType::PARAM_LIST_TITLE_ICON_PATH => $nodeLinkType->getListTitleIconPath(),
      NodeLinkType::PARAM_LIST_TABLE_FIELDS => $nodeLinkType->getListTableFields(),
    ];

    $storage[$nodeLinkType->getType()][MENOCI_NODELINK_VAR_STORAGE_KEY_CONFIG] = $data;
    variable_set(MENOCI_NODELINK_VAR_TYPE_STORAGE, $storage);
  }

  /**
   * @param string $node_type
   * @param int $publication_id
   *
   * @return array
   */
  public static function getLinkedNodeIds(string $node_type, int $publication_id) {

    $storage = variable_get(MENOCI_NODELINK_VAR_TYPE_STORAGE, []);
    if (!array_key_exists(MENOCI_NODELINK_VAR_STORAGE_KEY_LINKINGS, $storage[$node_type])) {
      return [];
    }
    $data = $storage[$node_type][MENOCI_NODELINK_VAR_STORAGE_KEY_LINKINGS];
    if (in_array($publication_id, array_keys($data))) {
      return $data[$publication_id];
    }
    else {
      return [];
    }
  }

  /**
   * @param string $node_type
   * @param int $publication_id
   * @param array $node_ids
   */
  public static function storeLinking($node_type, $publication_id, $node_ids) {
    $storage = variable_get(MENOCI_NODELINK_VAR_TYPE_STORAGE, []);
    $storage[$node_type][MENOCI_NODELINK_VAR_STORAGE_KEY_LINKINGS][$publication_id] = $node_ids;
    variable_set(MENOCI_NODELINK_VAR_TYPE_STORAGE, $storage);
  }
}