<?php


class NodeLinkTypeRenderer {

  private $nodelinktype;

  /**
   * NodeLinkTypeRenderer constructor.
   *
   * @param \NodeLinkType $nodelinktype
   */
  public function __construct(NodeLinkType $nodelinktype) {
    $this->nodelinktype = $nodelinktype;
  }

  /**
   * @return array
   */
  public function header() {
    $fields = $this->nodelinktype->getListTableFields();

    $labels = $this->getTableHeaderFieldMapping();
    $header = [];
    foreach ($fields as $field) {
      $header[] = $labels[$field];
    }

    $header[] = '<!-- Actions -->';
    return $header;
  }

  private function getTableHeaderFieldMapping() {
    $node_type_spec = node_type_get_type($this->nodelinktype->getType());
    $labels = [
      'node_title' => $node_type_spec->title_label,
      'node_created' => 'Created',
      'node_changed' => 'Changed',
      'node_uid' => 'Created by',
    ];
    $fields = field_read_instances(['entity_type' => 'node', 'bundle' => $this->nodelinktype->getType()]);
    foreach ($fields as $field) {
      $labels[$field['field_name']] = $field['label'];
    }
    return $labels;
  }

  /**
   * @param $fields
   *
   * @return array
   */
  public function row($node_id) {
    $fields = $this->nodelinktype->getListTableFields();

    $row = [];
    $node = entity_load('node', [$node_id])[$node_id];

    foreach ($fields as $field) {
      switch ($field) {
        case "node_title":
          $row[] = $node->title;
          break;
        case "node_created":
          $row[] = format_date($node->created, "custom", "Y-m-d");
          break;
        case "node_changed":
          $row[] = format_date($node->changed, "custom", "Y-m-d");
          break;
        case "node_uid":
          $row[] = UsersRepository::findByUid($node->uid)->getFullNameWithOrcid();
          break;
        default:
          $row[] = $this->fetch_field($field, $node_id);
          break;
      }
    }

    /**
     * Add view link
     */
    $row[] = l('<span class="glyphicon glyphicon-eye-open"></span>', 'node/' . $node_id, ['html' => TRUE]);
    return $row;
  }

  /**
   * @param $field
   * @param $node_id
   *
   * @return mixed|bool Field value on success, FALSE otherwise
   */
  private function fetch_field($field, $node_id) {

    try {
      $table = "field_data_" . $field;
      $value = $field . "_value";
      $query = db_select($table, "t");
      $query->fields("t", [$value]);
      $query->condition("t.entity_type", "node", "=");
      $query->condition("t.bundle", $this->nodelinktype->getType(), "=");
      $query->condition("t.entity_id", $node_id, "=");

      $result = $query->execute();
      $result = $result->fetchAssoc();

      return $result[$value];
    } catch (Exception $exception) {
      watchdog(__CLASS__, $exception->getMessage(), [], WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * @return string
   */
  public function headline() {

    if (!empty($src = $this->nodelinktype->getListTitleIconPath())) {
      $img = theme_image(['path' => $src, 'attributes' => ['style' => 'width: 32px; max-width: 32px;']]);
      $img .= '&nbsp;';
    }
    else {
      $img = '';
    }

    return '<h4>' . $img . $this->nodelinktype->getListTitle() . '</h4>';
  }

  public function getLinkingFormField($publication_id) {

    $default_values = $this->nodelinktype->getLinkedNodeIds($publication_id);

    $options = [];

    $entities = entity_load('node', FALSE, ['type' => $this->nodelinktype->getType()]);
    foreach ($entities as $node) {
      $options[$node->nid] = $node->title;
    }

    asort($options);

    // Needed for select 2 fields to work.
    sfb_commons_add_select2('', '', '', '');
    drupal_add_js('jQuery(document).ready(function() {
            jQuery(".field_select2").select2();
          });', 'inline');

    $field = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->nodelinktype->getListTitle(),
      '#options' => $options,
      '#default_value' => $default_values,
      '#attributes' => ['class' => ['field_select2'], 'multiple' => 'multiple'],
    ];

    return $field;
  }
}