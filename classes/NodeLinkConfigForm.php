<?php


class NodeLinkConfigForm {

  const FIELDSET_CURRENT_SETTINGS = 'current_settings';

  const FIELDNAME_LIST_MARKUP = 'list';

  const FIELDNAME_ADD_TYPE = 'add_type';

  public static function getForm() {

    /**
     * Fetch previously configured node types
     */
    $linked_types = variable_get(MENOCI_NODELINK_VAR_LINKED_TYPES, []);
    /**
     * Fetch all existing node types
     */
    $node_types = node_type_get_types();

    /**
     * Display an overview of the configured node types (if there are any)
     */
    if (count($linked_types)) {
      $form[self::FIELDSET_CURRENT_SETTINGS] = ['#type' => 'fieldset', '#title' => t("Current Settings")];

      $currently_linked_types = [];
      foreach ($linked_types as $key) {
        $currently_linked_types[] = $node_types[$key];
      }
      $list = '';
      foreach ($currently_linked_types as $item) {
        $link = l($item->name, NodeLinkType::url_by_type($item->type));
        $list .= '<li>' . $link . '</li>';
      }
      $list = '<h5>Content types currently available for linking:</h5><p><ul>' . $list . '</ul></p>';

      $form[self::FIELDSET_CURRENT_SETTINGS][self::FIELDNAME_LIST_MARKUP] = [
        '#type' => 'markup',
        '#markup' => $list,
      ];
    }

    /**
     * Display an input field to choose a new node type for configuration as linkable type.
     */
    $options = [];
    $blacklist = array_merge(['static_page'], $linked_types);
    foreach ($node_types as $key => $object) {
      if (!$object->disabled && !in_array($key, $blacklist)) {
        $options[$key] = $object->name;
      }
    }
    if (count($options)) {
      $form[self::FIELDNAME_ADD_TYPE] = [
        '#type' => 'select',
        '#title' => 'Available Content Types',
        '#options' => $options,
        '#empty_option' => "-- SELECT --",
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t("Save settings"),
      ];
    }

    return $form;
  }

  /**
   * @param array $values
   *
   * @return bool|mixed Returns the machine name of the added content type on success, FALSE otherwise
   */
  public static function handleSubmit($values) {
    if (array_key_exists(self::FIELDNAME_ADD_TYPE, $values) && $add = $values[self::FIELDNAME_ADD_TYPE]) {
      $linked_types = variable_get(MENOCI_NODELINK_VAR_LINKED_TYPES, []);
      array_push($linked_types, $add);
      variable_set(MENOCI_NODELINK_VAR_LINKED_TYPES, $linked_types);
      return $add;
    }
    else {
      return FALSE;
    }
  }
}