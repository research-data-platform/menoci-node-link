<?php


class NodeLinkType {

  const PARAM_LIST_TITLE = 'list_title';

  const PARAM_LIST_TITLE_ICON_PATH = 'list_title_icon_path';

  const PARAM_LIST_TABLE_FIELDS = 'list_table_fields';

  private $type;

  private $params = [];

  /**
   * NodeLinkType constructor.
   *
   * @param string $type
   */
  public function __construct(string $type) {
    $this->type = $type;
  }

  public function save() {
    NodeLinkTypeRepository::save($this);
  }

  public function getListTitle() {
    return $this->getParam(self::PARAM_LIST_TITLE);
  }

  private function getParam(string $param) {
    return array_key_exists($param, $this->params) ? $this->params[$param] : NULL;
  }

  public function setListTitle($value) {
    $this->setParam(self::PARAM_LIST_TITLE, $value);
  }

  private function setParam(string $param, $value) {
    $this->params[$param] = $value;
  }

  public function getListTitleIconPath() {
    return $this->getParam(self::PARAM_LIST_TITLE_ICON_PATH);
  }

  public function setListTitleIconPath($value) {
    $this->setParam(self::PARAM_LIST_TITLE_ICON_PATH, $value);
  }

  public function getListTableFields() {
    return $this->getParam(self::PARAM_LIST_TABLE_FIELDS);
  }

  public function setListTableFields($value) {
    $this->setParam(self::PARAM_LIST_TABLE_FIELDS, $value);
  }

  public function url($action = 'edit') {
    return self::url_by_type($this->getType(), $action);
  }

  public static function url_by_type($type, $action = 'edit') {
    $url = MENOCI_NODELINK_URL_CONFIG;
    switch ($action) {
      case 'edit':
      default:
        $url = str_replace('%', $type, MENOCI_NODELINK_URL_CONFIG_TYPE_EDIT);
        break;
    }
    return $url;
  }

  public function getType() {
    return $this->type;
  }

  public function getLinkedNodeIds($publication_id) {
    return NodeLinkTypeRepository::getLinkedNodeIds($this->getType(), $publication_id);
  }
}