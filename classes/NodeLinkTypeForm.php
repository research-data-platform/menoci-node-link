<?php


class NodeLinkTypeForm {

  const FIELDNAME_LIST_TITLE = 'list_title';

  const FIELDNAME_LIST_TITLE_ICON_PATH = 'list_title_icon_path';

  const FIELDNAME_LIST_TABLE_FIELDS = 'list_table_fields';

  /**
   * @var \NodeLinkType
   */
  private $nodelinktype;

  /**
   * NodeLinkTypeForm constructor.
   *
   * @param \NodeLinkType $nodelinktype
   */
  public function __construct(NodeLinkType $nodelinktype) {
    $this->nodelinktype = $nodelinktype;
  }

  public function getEditForm() {
    $form = [];

    $form[self::FIELDNAME_LIST_TITLE] = [
      '#type' => 'textfield',
      '#title' => 'Linking Display List Header Text',
      '#required' => TRUE,
      '#default_value' => $this->nodelinktype->getListTitle(),
    ];

    $form[self::FIELDNAME_LIST_TITLE_ICON_PATH] = [
      '#type' => 'textfield',
      '#title' => 'Linking Display List Header Icon',
      '#default_value' => $this->nodelinktype->getListTitleIconPath(),

    ];

    $options_table_fields = $this->getOptionsTableFields();

    // Needed for select 2 fields to work.
    sfb_commons_add_select2('', '', '', '', 0);
    drupal_add_js('jQuery(document).ready(function() {
            jQuery(".field_select2").select2();
          });', 'inline');

    $form[self::FIELDNAME_LIST_TABLE_FIELDS] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Display Table Fields'),
      '#options' => $options_table_fields,
      '#attributes' => ['class' => ['field_select2'], 'multiple' => 'multiple'],
      '#default_value' => $this->nodelinktype->getListTableFields(),
    ];


    $form['submit'] = ['#type' => 'submit', '#value' => 'Save type configuration'];

    return $form;
  }

  public function getOptionsTableFields() {

    $node_type_spec = node_type_get_type($this->nodelinktype->getType());
    $options = [
      'node_title' => $node_type_spec->title_label,
      'node_created' => 'Created',
      'node_changed' => 'Changed',
      'node_uid' => 'Created by',
    ];
    $fields = field_read_instances(['entity_type' => 'node', 'bundle' => $this->nodelinktype->getType()]);
    foreach ($fields as $field) {
      $options[$field['field_name']] = $field['label'] . " (" . $field['field_name'] . ")";
    }
    return $options;
  }

  public function handleEditFormSubmit($values) {
    $result = [$values[self::FIELDNAME_LIST_TITLE], $values[self::FIELDNAME_LIST_TABLE_FIELDS]];

    $this->nodelinktype->setListTitle($values[self::FIELDNAME_LIST_TITLE]);
    $this->nodelinktype->setListTitleIconPath($values[self::FIELDNAME_LIST_TITLE_ICON_PATH]);
    $this->nodelinktype->setListTableFields(array_keys(array_filter($values[self::FIELDNAME_LIST_TABLE_FIELDS])));
    $this->nodelinktype->save();
  }
}